import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import { useDispatch } from "react-redux";
import LoginScreen from "../screens/LoginScreen";
import MapsView from "../src/MapsView";
import GooglePlacesInput from "../src/GooglePlacesInput";
import Dashboard, { DashboardScreenOptions } from "../screens/Dashboard";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Ionicons from "react-native-vector-icons/Ionicons";
import {
  createDrawerNavigator,
  DrawerItemList,
} from "@react-navigation/drawer";
import Color from "../Color/Color";
import Metode from "../screens/Metode";
import Help,{HelpScreenOptions} from "../screens/Help";
import About,{AboutScreenOptions} from "../screens/About";
import Info,{InfoOption} from "../screens/Info";
import DetailScreen,{DetailScreenOptions} from "../screens/DetailScreen";
import * as authaction from "../store/action";

import { Platform, SafeAreaView, Button, View } from "react-native";

const LoginStack = createStackNavigator();
const NavigationLogin = () => {
  return (
    <LoginStack.Navigator headerMode="none">
      <LoginStack.Screen name="LoginScreen" component={LoginScreen} />
      <LoginStack.Screen name="Dashboard" component={MealsFavTabNavigator} />
      <LoginStack.Screen name="Homes" component={MapsView} />
      <LoginStack.Screen name="PlacesInput" component={GooglePlacesInput} />
    </LoginStack.Navigator>
  );
};

const DashboardStack = createStackNavigator();
const DashboardNavigation = () => {
  return (
    <DashboardStack.Navigator>
      <DashboardStack.Screen
        name="Dashborad"
        component={Dashboard}
        options={DashboardScreenOptions}
      />
      <DashboardStack.Screen
        name="Detail"
        component={DetailScreen}
        options={DetailScreenOptions}
      />
    </DashboardStack.Navigator>
  );
};

const HelpStack = createStackNavigator();
const HelpNavigation = () => {
  return (
    <HelpStack.Navigator>
      <HelpStack.Screen
        name="Help"
        component={Help}
        options={HelpScreenOptions}
      />
    </HelpStack.Navigator>
  );
};

const AboutStack = createStackNavigator();
const AboutNavigation = () => {
  return (
    <AboutStack.Navigator>
      <AboutStack.Screen
        name="About"
        component={About}
        options={AboutScreenOptions}
      />
    </AboutStack.Navigator>
  );
};

const InfoStack = createStackNavigator();
const InfoNavigation = () => {
  return (
    <InfoStack.Navigator>
      <InfoStack.Screen
        name="Info"
        component={Info}
        options={InfoOption}
      />
    </InfoStack.Navigator>
  );
};

const Drawer = createDrawerNavigator();
const MainNavigator = () => {
  const dispatch = useDispatch()
 
  return (
    <Drawer.Navigator
      drawerContentOptions={{
        activeTintColor: Color.primaryColor,
      }}
      drawerContent={(props) => {
        return (
          <View style={{ flex: 1, padding: 20 }}>
            <SafeAreaView forceInset={{ top: "always", horizontal: "never" }}>
              <DrawerItemList {...props} />
              <Button
                title="Logout"
                color={Color.accentColor}
                onPress={() => {
                  dispatch(authaction.logout());
                  props.navigation.navigate("LoginScreen");
                }}
              />
            </SafeAreaView>
          </View>
        );
      }}
    >
      <Drawer.Screen
        name="Dashboard"
        component={DashboardNavigation}
        options={{
          drawerIcon: (props) => (
            <Ionicons
              name={Platform.OS === "android" ? "md-list" : "ios-pizza"}
              size={23}
              color={props.color}
            />
          ),
        }}
      />
      {/* <Drawer.Screen
        name="Info"
        component={InfoNavigation}
        options={{
          drawerIcon: (props) => (
            <Ionicons
              name={Platform.OS === "android" ? "bookmarks-outline" : "ios-pizza"}
              size={23}
              color={props.color}
            />
          ),
        }}
      /> */}
      {/* <Drawer.Screen
        name="Help"
        component={HelpNavigation}
        options={{
          drawerIcon: (props) => (
            <Ionicons
              name={Platform.OS === "android" ? "md-warning" : "ios-pizza"}
              size={23}
              color={props.color}
            />
          ),
        }}
      /> */}
      {/* <Drawer.Screen
        name="About"
        component={AboutNavigation}
        options={{
          drawerIcon: (props) => (
            <Ionicons
              name={Platform.OS === "android" ? "airplane-outline" : "ios-pizza"}
              size={23}
              color={props.color}
            />
          ),
        }}
      /> */}
    </Drawer.Navigator>
  );
};

const Tab = createBottomTabNavigator();
const MealsFavTabNavigator = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeColor: Color.accentColor,
        barStyle: {
          backgroundColor: Color.accentColor,
        },
      }}
    >
      <Tab.Screen
        name="Dashboard"
        component={MainNavigator}
        options={{
          tabBarIcon: (tabInfo) => {
            return (
              <Ionicons
                name="apps-outline"
                size={25}
                color={Color.accentColor}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Direction"
        component={MapsView}
        options={{
          tabBarIcon: (tabInfo) => {
            return (
              <Ionicons
                name="compass-outline"
                size={25}
                color={Color.accentColor}
              />
            );
          },
        }}
      />

      <Tab.Screen
        name="Info"
        component={InfoNavigation}
        options={{
          tabBarIcon: (tabInfo) => {
            return (
              <Ionicons
                name="checkbox-outline"
                size={25}
                color={Color.accentColor}
              />
            );
          },
        }}
      />

    {/* <Tab.Screen
        name="About"
        component={AboutNavigation}
        options={{
          tabBarIcon: (tabInfo) => {
            return (
              <Ionicons
                name="hourglass-outline"
                size={25}
                color={Color.accentColor}
              />
            );
          },
        }}
      /> */}

      {/* <Tab.Screen
        name="Metode"
        component={Metode}
        options={{
          tabBarIcon: (tabInfo) => {
            return (
              <Ionicons
                name="hourglass-outline"
                size={25}
                color={Color.accentColor}
              />
            );
          },
        }}
      /> */}
    </Tab.Navigator>
  );
};

export default Navigations = () => {
  return (
    <NavigationContainer>
      <NavigationLogin />
    </NavigationContainer>
  );
};
