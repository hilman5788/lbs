import React, { useState, useEffect, useCallback } from "react";
import {
  View,
  Text,
  FlatList,
  Button,
  Platform,
  ActivityIndicator,
  StyleSheet,
  ScrollView,
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import Card from "../components/Card";
import ProductItem from "../components/ProductItem";
import { DrawerActions } from "@react-navigation/native";
import Color from "../Color/Color";

const About = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const products = [
    {
      id: 1,
      description:
        "Dalam catatan geologi, ada sebuah Superbenua bernama Pangea. Superbenua ini terpecah sekitar 180 juta tahun lalu dan membentuk 2 benua raksasa (Laurasia dan Gondwana).",
      imageUrl:
        "https://anekatempatwisata.com/wp-content/uploads/2016/08/Gunung-Bromo-Jawa-Timur.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      price: "2200",
      title: "coba aja About",
    },
    {
      id: 2,
      description:
        "Gunung Burangrang memiliki 3 Jalur Pendakian yaitu Legok Haji, Pos Komando, dan Pangheotan, dimana masing-masing jalur",
      imageUrl:
        "https://anekatempatwisata.com/wp-content/uploads/2016/08/Gunung-Prau-Jawa-Tengah.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      price: "2200",
      title: "coba aja About",
    },
    {
      id: 3,
      description:
        "Bukit Tunggul Bandung, atau banyak juga yang menyebutnya Bukit unggul Bandung merupakan salah satu taman wisata alam alternatif yang cocok untuk menjadi tempat wisata alternatif di Bandung",
      imageUrl:
        "https://anekatempatwisata.com/wp-content/uploads/2016/08/Gunung-Rinjani-NTB.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      price: "2200",
      title: "coba aja About",
    },
    {
      id: 4,
      description:
        "Gunung Mandalawangi adalah sebuah gunung di provinsi Banten. Letaknya di Kabupaten Pandeglang. Gunung Mandalawangi memiliki ketinggian 588 meter dari permukaan laut",
      imageUrl:
        "https://anekatempatwisata.com/wp-content/uploads/2016/08/Gunung-Bromo-Jawa-Timur.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      price: "2200",
      title: "coba aja About",
    },
    {
      id: 5,
      description:
        "Gunung Kendang adalah gunung berapi stratovolcano di Jawa Barat, Indonesia. Gunung Kendang terdapat empat lapangan Fumarol termasuk Kawah Manuk, dengan luas kawah 2,75 km",
      imageUrl:
        "https://americansfortransit.org/wp-content/uploads/2020/06/landscape-3779159_640.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      price: "2200",
      title: "coba aja About",
    },
  ];
  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  return (
    <FlatList
      data={products}
      keyExtractor={(item) => item.id.toString()}
      renderItem={(itemData) => (
        <ProductItem
          image={itemData.item.imageUrl}
          price={itemData.item.description}
        >
        </ProductItem>
      )}
    />
  );

};

export const AboutScreenOptions = (navData) => {
  return {
    headerTitleStyle: { alignItems: 'center'},
    headerStyle: {
      backgroundColor: Platform.OS === "android" ? Color.primaryColor : "",
    },
    headerTintColor: '#fff',
    headerLeft: () => {
      return (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
            title="Menu"
            iconName={Platform.OS === "android" ? "md-menu" : "ios-menu"}
            onPress={() => {
              navData.navigation.dispatch(DrawerActions.openDrawer());
            }}
          />
        </HeaderButtons>
      );
    },
  };
};

const styles = StyleSheet.create({
  centered: { flex: 1, justifyContent: "center", alignItems: "center" },
});

export default About;
