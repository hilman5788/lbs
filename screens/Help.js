import React, { useState, useEffect, useCallback } from "react";
import {
  View,
  Text,
  FlatList,
  Button,
  Platform,
  ActivityIndicator,
  StyleSheet,
  ScrollView,
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import Card from "../components/Card";
import ProductItem from "../components/ProductItem";
import { DrawerActions } from "@react-navigation/native";
import Color from "../Color/Color";

const Help = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const products = [
    {
      id: 1,
      description:
        "menu ini berisi tentang about dimana about ini menceritakan tentang tujuan pembuatan gunung menu ini berisi tentang about dimana about ini menceritakan tentang tujuan pembuatan gunung menu ini berisi tentang about dimana about ini menceritakan tentang tujuan pembuatan gunung menu ini berisi tentang about dimana about ini ",
      imageUrl:
        "https://d99i6ad9lbm5v.cloudfront.net/uploads/image/file/5516/gunung-terindah-di-indonesia-1.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      price: "2200",
      title: "coba aja About",
    },
    {
      id: 2,
      description:
        "menu ini berisi tentang about dimana about ini menceritakan tentang tujuan pembuatan gunung menu ini berisi tentang about dimana about ini menceritakan tentang tujuan pembuatan gunung menu ini berisi tentang about dimana about ini menceritakan tentang tujuan pembuatan gunung menu ini berisi tentang about dimana about ini ",
      imageUrl:
        "https://d99i6ad9lbm5v.cloudfront.net/uploads/image/file/5516/gunung-terindah-di-indonesia-1.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      price: "2200",
      title: "coba aja About",
    },
    {
      id: 3,
      description:
        "menu ini berisi tentang about dimana about ini menceritakan tentang tujuan pembuatan gunung menu ini berisi tentang about dimana about ini menceritakan tentang tujuan pembuatan gunung menu ini berisi tentang about dimana about ini menceritakan tentang tujuan pembuatan gunung menu ini berisi tentang about dimana about ini ",
      imageUrl:
        "https://d99i6ad9lbm5v.cloudfront.net/uploads/image/file/5516/gunung-terindah-di-indonesia-1.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      price: "2200",
      title: "coba aja About",
    },
  ];
  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  return (
    <FlatList
      data={products}
      keyExtractor={(item) => item.id.toString()}
      renderItem={(itemData) => (
        <ProductItem
          image={itemData.item.imageUrl}
          price={itemData.item.description}
        >
        </ProductItem>
      )}
    />
  );

};

export const HelpScreenOptions = (navData) => {
  return {
    headerTitleStyle: { alignItems: 'center'},
    headerStyle: {
      backgroundColor: Platform.OS === "android" ? Color.accentColor : "",
    },
    headerTintColor: '#fff',
    headerLeft: () => {
      return (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
            title="Menu"
            iconName={Platform.OS === "android" ? "md-menu" : "ios-menu"}
            onPress={() => {
              navData.navigation.dispatch(DrawerActions.openDrawer());
            }}
          />
        </HeaderButtons>
      );
    },
  };
};

const styles = StyleSheet.create({
  centered: { flex: 1, justifyContent: "center", alignItems: "center" },
});

export default Help;
