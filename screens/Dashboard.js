import React, { useState, useEffect, useCallback } from "react";
import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  FlatList,
  TextInput,
  SafeAreaView,
  ScrollView,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { SliderBox } from "react-native-image-slider-box";
const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;
import Color from "../Color/Color";
import { useSelector, useDispatch } from "react-redux";
import Icon from "react-native-vector-icons/Ionicons";
const Dashboard = (props) => {
  const authuser = useSelector((state) => state.auth.loginUser);
  const authadmin = useSelector((state) => state.auth.loginAdmin);
  console.log("authuser", authuser);
  console.log("authadmin", authadmin);
  const [isLoading, setIsLoading] = useState(false);
  const images = [
    "https://irmadevita.com/wp-content/uploads/2020/02/jl-tanjungpura-1971.jpg",
    "http://2.bp.blogspot.com/-I1hxu9BR-hk/Te8gerJO3MI/AAAAAAAAAFg/zCHxgn7RNN4/s1600/Museum+Sejarah+Jakarta.jpg",
    "http://2.bp.blogspot.com/-C4bFrif5yIE/UR0ccQrTUWI/AAAAAAAABL0/eDsazWnIHeQ/s400/CE25F68109F38BDB4418E3FCD4D19.jpg",
    "https://asset-a.grid.id//crop/0x0:0x0/700x465/photo/2018/10/24/1234160880.jpg",
    "http://blog.ub.ac.id/yolandarigustira/files/2013/10/Wisata-Sejarah-Jakarta-Solusi-Liburan-Murah-Keluarga.jpg",
  ];
  const products = [
    {
      id: 1,
      description:
      "",
      // "Museum ini merupakan satu-satunya tempat bersejarah di Jakarta Selatan yang populer dan ramai dikunjungi orang, lho. Sejarahnya, museum ini merupakan kediaman Ratna Sari Dewi, istri presiden pertama RI, Soekarno. Rumah ini bahkan menjadi tempat persemayaman terakhir Presiden Soekarno sebelum dikuburkan di kota Blitar. Saat Presiden Soeharto berkuasa, pada tahun 1971 rumah ini resmi menjadi Museum Satria Mandala dan dialihfungsikan sebagai pusat sejarah TNI .        Museum ini berlokasi di Jl. Jend. Gatot Subroto No. 14, RT.6/RW.1, Kuningan Barat, Jakarta Selatan.",
      imageUrl:
        "https://www.nativeindonesia.com/wp-content/uploads/2019/04/Taman-Mini-Indonesia-Indah.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      title: "Museum Satria Mandala, Jakarta Selatan",
    },
    {
      id: 2,
      description:
      "",  
      // "Monas atau Monumen Nasional merupakan simbol salah satu simbol ibukota yang wajib Anda kunjungi kala berada di Provinsi ini. Monumen ini pertama kali di bangun pada era Presiden pertama republik Indonesia. Puncak dari Monas terdapat di tengah taman yang sangat luas dan menjulang tinggi. Anda bisa masuk dan sedikit mengantri menuju puncak dari monumen ini untuk melihat Jakarta dari ketinggihan. Di area bawah dari bangunan ini terdapat aula yang berisikan aneka foto perjuangan dan lain sebagainya yang bisa Anda nikmati. Konon katanya puncak dari monumen ini terbuat dari emas asli",
      imageUrl:
        "https://www.ainonholidays.co.id/wp-content/uploads/2019/12/Liburan-Asyik-di-Monas-Untuk-Para-Traveller.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      title: "Monumen Nasional (Monas)",
    },
    {
      id: 3,
      description:
      "",
      // "Nah, buat para Toppers yang ingin wisata sejarah sambil foto-foto bernuansa vintage, museum inilah jawabannya. Tempat bersejarah di Jakarta ini dulu merupakan bangunan bekas komplek gudang untuk menyimpan rempah seperti pala, tembakau, kopra, lada, dan berbagai rempah lainnya. Banyak koleksi yang dapat dilihat di museum Bahari. Salah satunya adalah model-model kapal milik Belanda lengkap dengan meriamnya.",
      imageUrl:
        "https://asset.kompas.com/crops/FuWWXjWJNT_C_aon_DMvqf0m21Q=/0x40:1000x707/750x500/data/photo/2018/01/23/2705586670.jpeg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      title: "Museum Bahari, Jakarta Utara",
    },
    {
      id: 4,
      description:
     "",
      //  "Di Museum Bank Indonesia, kamu bisa menemukan benda dan dokumen bersejarah yang dirawat mengedukasi generasi penerus tentang sejarah Jakarta dan Indonesia.  Di tempat bersejarah satu ini, Toppers juga bisa menemukan diorama menarik yang menceritakan perekonomian Indonesia dari waktu ke waktu       Selain itu, kamu juga bisa melihat mata uang rupiah dari masa ke masa serta peninggalan sejarah lainnya.",
      imageUrl:
        "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/08/Museum-Bank-Indonesia.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      title: "Museum Bank Indonesia, Jakarta Barat",
    },
    {
      id: 5,
      description:
      "",
      //  "Sebelum menjadi museum Kebangkitan Nasional, bangunan ini adalah sekolah kedokteran di era kolonial Belanda bernama STOVIA atau School tot Opleding van Inlandsche Arsten. Tempat ini diresmikan pada tahun 1902.       Di tempat yang sangat bersejarah ini, terdapat beberapa diorama sebagai koleksi museum seperti guru STOVIA, pelajar STOVIA bahkan diorama mengenai Kartini. ",
      imageUrl:
        "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/08/Museum-Kebangkitan-Nasional.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      title: "Museum Kebangkitan Nasional, Jakarta Pusat",
    },
    {
      id: 6,
      description:
       "",
      // "Selain sebagai tempat beribadah umat Katolik, Gereja Katedral juga berfungsi sebagai tempat wisata sejarah, lho Toppers  Katedral diresmikan pada 28 April 1991 oleh Mgr. Julius Darmaatmadja. Koleksinya berupa sejarah para misionaris dari Belanda yang ditugaskan untuk menyebarkan agama Katolik beserta dengan deretan foto Kardinal dari waktu ke waktu.        Museum ini berada di Jl. Katedral, Pasar Baru, Jakarta Pusat. Jika kamu ingin berkunjung, museum dibuka setiap hari Senin sampai Kamis dan Sabtu pada pukul 10.00 hingga 16.00 dan untuk hari Minggu dimulai pada pukul 12.00 hingga 16.00. ",
      imageUrl:
        "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/08/Museum-Katedral.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      title: "Museum Katedral, Jakarta Pusat",
    },
    {
      id: 7,
      description:
      "",  
      // "Kota tua adalah tempat bersejarah di Jakarta yang merupakan objek wisata menarik. Di Kota Tua kamu bisa menemukan banyak tempat untuk dikunjungi mulai dari museum Fatahillah, Pelabuhan Sunda Kelapa, Museum Bank Indonesia, Museum Wayang, dan Toko Merah. Kota Tua ini berlokasi di Jalan Pinangsia, Taman Sari, Jakarta Barat.",
      imageUrl:
        "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/08/Kota-Tua.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      title: "Kota Tua, Jakarta Barat",
    },
    {
      id: 8,
      description:
      "",  
      // "Museum Nasional Indonesia atau lebih dikenal dengan nama Museum Gajah didirikan oleh pemerintah kolonial Belanda pada tahun 1978.Koleksi yang berada di museum ini berjumlah lebih dari 140.000 yang dibagi menjadi beberapa bagian yaitu koleksi zaman pra-sejarah, arkeologi, keramik, etnografi, dan geografis. Museum ini berlokasi di Jl. Merdeka Barat No. 12, Jakarta Pusat yang dibuka dari jam 08.30 sampai 14.30 pada hari Selasa, Rabu, Kamis, dan Minggu.",
      imageUrl:
        "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/08/Museum_Gajah.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      title: "Museum Gajah, Jakarta Pusat",
    },
    {
      id: 9,
      description:
       "",
      // "Tugu ini pertama kali dibangun pada 1 Januari 1961 dan diresmikan oleh Presiden Soeharto pada 17 Agustus 1972.Di tempat ini kamu bisa mengenang sejarah Proklamasi Kemerdekaan RI, Toppers. Tidak hanya tempat wisata saja, lokasi ini juga sering dijadikan sebagai tempat upacara kemerdekaan, lho.Lokasi Tugu Proklamasi ini berada di Jl. Proklamasi, RT.10/RW.2, Pegangsaan, Menteng, Jakarta Pusat. ",
      imageUrl:
        "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/08/Tugu-Proklamasi.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      title: "Tugu Proklamasi, Jakarta Pusat",
    },
    {
      id: 10,
      description:
       "",
      // "Kota tua adalah tempat bersejarah di Jakarta yang merupakan objek wisata menarik. Di Kota Tua kamu bisa menemukan banyak tempat untuk dikunjungi mulai dari museum Fatahillah, Pelabuhan Sunda Kelapa, Museum Bank Indonesia, Museum Wayang, dan Toko Merah. Kota Tua ini berlokasi di Jalan Pinangsia, Taman Sari, Jakarta Barat.",
      imageUrl:
        "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/08/Kota-Tua.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      title: "Kota Tua, Jakarta Barat",
    },
  ];

  const selectItemHandler = (id, title, imageUrl, description, price) => {
    //console.log(id)
    props.navigation.navigate("Detail", {
      id: id,
      title: title,
      imageUrl: imageUrl,
      description: description,
      price: price,
    });
  };

  const CourseCard = (course) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() =>
          selectItemHandler(
            course.course.id,
            course.course.title,
            course.course.imageUrl,
            course.course.description,
            course.course.price
          )
        }
      >
        <ImageBackground
          source={{ uri: course.course.imageUrl }}
          style={{
            marginVertical: 10,
            marginHorizontal: 5,
            borderRadius: 50,
            width: windowWidth / 2 - 30,
            height: windowHeight / 3,
            paddingTop: 25,
            paddingLeft: 20,
            borderRadius: 15,
            overflow: "hidden",
          }}
        >
          <Text
            style={{
              fontSize: 15,
              fontWeight: "bold",
              paddingBottom: 5,
              color: "#33b5e5",
              opacity:100,
              // backgroundColor: "#F8F8FF",
              textAlign:"center",
              marginRight: 20
            }}
          >
            {course.course.title}
          </Text>
          {/* <Text style={{ color: "white", fontWeight: "bold" }}>
            {course.course.price + " Tiket Masuk"}
          </Text> */}
        </ImageBackground>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView
      style={{
        backgroundColor: "#fff",
        flex: 1,
        paddingHorizontal: 20,
      }}
    >
      <ScrollView>
        <View style={{ marginTop: 20 }}>
          <Text style={{ fontSize: 28, fontWeight: "bold" }}>
            SEJARAH TERBENTUKNYA KOTA JAKARTA
          </Text>
          <View
            style={{
              paddingVertical: 25,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          ></View>
        </View>
        <SliderBox
          images={images}
          sliderBoxHeight={200}
          onCurrentImagePressed={(index) => console.log(index)}
          dotColor="#FFEE58"
          inactiveDotColor="#90A4AE"
          paginationBoxVerticalPadding={20}
          autoplay
          circleLoop
          resizeMethod={"resize"}
          resizeMode={"cover"}
          paginationBoxStyle={{
            position: "absolute",
            bottom: 0,
            padding: 0,
            alignItems: "center",
            alignSelf: "center",
            justifyContent: "center",
            paddingVertical: 10,
          }}
          dotStyle={{
            width: 10,
            height: 10,
            borderRadius: 5,
            marginHorizontal: 0,
            padding: 0,
            margin: 0,
            backgroundColor: "rgba(128, 128, 128, 0.92)",
          }}
          ImageComponentStyle={{
            borderRadius: 15,
            width: "90%",
            marginTop: 5,
            marginLeft: -50,
          }}
          imageLoadingColor="#2196F3"
        />

        <View style={{}}>
          <Text style={{ textAlign: "justify" }}>
            Hari Ulang Tahun (HUT) Ibu Kota DKI Jakarta diperingati setiap
            tanggal 22 Juni. Berawal dari keberhasilan Fatahillah mengusir
            Portugis dari Sunda Kelapa pada 22 Juni 1527, semenjak saat itu
            Sunda Kelapa berganti nama menjadi Jayakarta yang artinya
            `kemenangan yang nyata` atau `kemenangan yang sempurna`. Lalu,
            bagaimana sebenarnya sejarah HUT Kota Jakarta jatuh pada 22 Juni?
            Hari jadi Kota Jakarta pada 22 Juni ditetapkan pada massa
            pemerintahan Wali Kota Sudiro yang menjabat periode 1953 – 1958.
            Penetapan berdirinya Kota Jakarta merujuk pada sejarah perebutan
            pelabuhan Sunda Kelapa oleh tokoh Kerajaan Demak bernama Fatahillah
            tahun 1527. Mengutip Wikipedia, pada masa kolonial, Belanda
            memperingati hari jadi Kota Batavia tiap akhir Mei dengan dasar
            bahwa pada akhir Mei 1619, Gubernur Jenderal Jan Pieterszoon Coen
            menaklukkan Jayakarta. Pada tahun 1869, untuk memperingati 250 tahun
            usia Batavia, dibangun pula monument J. P. Coen – saat ini halaman
            Departemen Keuangan, Jalan Lapangan Banteng, Jakarta Pusat. Di atas
            fondasi beton yang kokoh, berdiri Coen yang dengan angkuhnya
            menunjuk kearah bawah – menggambarkan dia berhasil menaklukkan
            Jayakarta. Patung yang menjadi simbol dimulainya penjajahan Belanda
            itu dihancurkan pada masa pendudukan Jepang (1942-1945). Sudiro,
            menyadari perlunya peringatan ulang tahun untuk kota ini yang
            berbeda dengan peringatan berdirinya Batavia. Maka, ia pun memanggil
            sejumlah ahli sejarah, seperti Mr. Mohamad Yamin dan Mr. Dr. Sukanto
            serta wartawan senior Sudarjo Tjokrosiswoyo untuk meneliti kapan
            Jakarta didirikan oleh Fatahillah. Kala itu, Sudiro berkeyakinan
            bahwa tahunnya adalah pasti, yaitu 1527. Yang menjadi pertanyaan
            adalah hari, tanggal, dan bulan lahirnya Kota Jakarta. Mr. Dr.
            Sukanto menyerahkan naskah berjudul Dari Jayakarta ke Jakarta. Dia
            menduga bahwa 22 Juni 1527 adalah hari yang paling dekat pada
            kenyataan dibangunnya Kota Jayakarta oleh Fatahillah. Naskah
            tersebut kemudian diserahkan oleh Sudiro kepada Dewan Perwakilan
            Kota Sementara untuk dibahas, yang kemudian langsung bersidang dan
            menetapkan bahwa 22 Juni 1527 sebagai berdirinya Kota Jakarta. Tepat
            pada 22 Juni 1956, Sudiro mengajukannya dengan resmi pada sidang
            pleno dan usulnya itu diterima dengan suara bulat. Selanjutnya,
            sejak saat itu, tiap 22 Juni diadakan sidang istimewa DPRD Kota
            Jakarta sebagai tradisi memperingati berdirinya Kota Jakarta.
            Selanjutnya 18 Januari 1985, Kota Praja Jakarta diubah menjadi Kota
            Praja Djakarta Raya. Kemudian baru 1961 terbentuklah nama Pemerintah
            Daerah Khusus Ibu Kota Jakarta Raya hingga sekarang.
            ------------------------ Ada juga pendapat yang mengatakan bahwa
            dipilihnya 22 Juni 1527 karena saat itu merupakan Maulid Nabi
            Muhammad SAW. Setelah berhasil mengusir Portugis dari Sunda Kalapa,
            Fatahillah sebagai panglima Kesultanan Demak mengubah Sunda Kelapa
            menjadi Jayakarta. -------------------------- Menurut sejarawan
            Adolf Heyken SJ, hari jadi Jakarta hanyalah sebuah dongeng. Karena,
            katanya, tak ada dokumen yang menyebutkan nama Jayakarta. Bahkan 50
            tahun sesudahnya (saat VOC berkuasa), tetap disebut Sunda Kelapa.
            Fatahillah adalah orang Arab. Jelaslah tidak mungkin apabila orang
            Arab memberi nama sesuatu dengan bahasa Sanskerta. Jayakarta adalah
            nama dari bahasa Sanskerta. Jadi, itu semua dongeng supaya Jakarta
            memiliki hari ulang tahun. Dalam Prasasti Buku Tulis di Bogor,
            tertera telah berdiri Kerajaan Padjajaran pada 1133 dengan wilayah
            kekuasaan meliputi Tangerang, Jakarta, Bekasi, dan Bogor. Saat itu,
            Pelabuhan Sunda Kelapa di Jakarta Utara menjadi pusat transportasi
            air yang dikuasai Kerajaan Padjajaran. Ketika Portugis tiba tahun
            1522, telah terjadi perjanjian dagang dan pertahanan antara Raja
            Padjajaran dengan Portugis. Inti perjanjian yang berlangsung pada 21
            Agustus 1522 itu, memberikan kebebasan kepada Portugis untuk
            berdagang melalui Pelabuhan Sunda Kelapa dan memberikan izin
            mendirikan benteng pertahanan. Tahun 1527, Portugis kembali datang
            ke Pelabuhan Sunda Kelapa untuk menindaklanjuti perjanjian pada
            1522. Akan tetapi, waktu itu Pelabuhan Sunda Kelapa sudah dikuasai
            tentara Kerajaan Demak di bawah pimpinan Fatahilah. Kemudian 22 Juni
            1527, Fatahillah dapat mengalahkan dan mengusir Portugis dari Sunda
            Kelapa. Kemudian mengganti nama pelabuhan tersebut menjadi
            Jayakarta. Berjalannya waktu, Belanda menduduki Jayakarta dan
            berganti nama menjadi Sta Batavia. Kemudian berubah lagi menjadi
            Gemeente Batavia pada 1905. Tahun 1942, setelah Jepang menduduki
            Batavia diubah menjadi Toko Betsu Shi. Lalu setelah Jepang menyerah
            kepada sekutu, namanya menjadi Pemerintah Nasional Kota Jakarta.
            Tidak berapa lama setelah itu, keseluruhan kota diduduki oleh
            pemerintahan NICA, namanya pun kembali seperti dulu, Stad Gemeente
            Batavia. Pada 24 Maret 1950, diubah kembali menjadi Kota Praja
            Jakarta. Selepas masa penjajahan, Wali Kota Sudiro menyadari Jakarta
            harus memiliki hari jadi. Dia kemudian mengumpulkan sejumlah tokoh,
            seperti Mohamad Yamin dan Sukanto, serta wartawan senior Sudarjo
            Tjokrosiswoyo untuk meneliti kapan Jakarta didirikan oleh
            Fatahillah. Sukanto menyerahkan naskah berjudul “Dari Jayakarta ke
            Jakarta”. Naskah itu kemudian diserahkannya ke Dewan Perwakilan Kota
            Sementara untuk dibahas. Digelarlah sidang dan menetapkan 22 Juni
            1527 sebagai berdirinya Kota Jakarta. Pada 22 Juni 1956, Sudiro
            mengajukannya dengan resmi dan usulannya diterima dengan suara
            bulat. Sejak saat itu, setiap 22 Juni diadakan sidang istimewa DPRD
            Kota Jakarta. Sebagai tradisi memperingati berdirinya Kota Jakarta.
            Selanjutnya 18 Januari 1985, Kota Praja Jakarta diubah menjadi Kota
            Praja Djakarta Raya. Kemudian baru 1961 terbentuklah nama Pemerintah
            Daerah Khusus Ibu Kota Jakarta Raya hingga sekarang.
          </Text>
        </View>
        <View style={{ marginTop: 20 }}>
          <Text style={{ fontSize: 28, fontWeight: "bold" }}>Wellcome</Text>
          <View
            style={{
              paddingVertical: 25,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <Text style={{ fontSize: 20, fontWeight: "bold" }}>
              List Wiata Jakarta
            </Text>
          </View>
        </View>
        <View style={{ flex: 1 }}>
          <FlatList
            showsVerticalScrollIndicator={false}
            numColumns={2}
            data={products}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <CourseCard course={item} style={{ fontColor: "#000" }} />
            )}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export const DashboardScreenOptions = (navData) => {
  return {
    headerTitle: "Jakarta Indonesia",
    headerTitleStyle: { alignItems: "center" },
    headerStyle: {
      backgroundColor: Platform.OS === "android" ? Color.accentColor : "",
    },
    headerLeft: null,
    headerTintColor: "#fff",
  };
};

export default Dashboard;
