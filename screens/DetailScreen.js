import React from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  ImageBackground,
  FlatList,
  SafeAreaView,
  Image,
} from "react-native";
import { useSelector,useDispatch } from "react-redux";
import Color from "../Color/Color";
import  Icon from "react-native-vector-icons/MaterialIcons";
const DetailScreen = (props) => {
  const {data} = props.route.params;
  const productId = props.route.params.id;
  console.log(productId)
  const imageUrl = props.route.params.imageUrl;
  const price = props.route.params.price;
  const title = props.route.params.title;
  const description = props.route.params.description;
  console.log(productId)
  const dispatch = useDispatch() 
  const BottomNavigationBar = () => {
    return (
      <View
        style={{
          height: 100,
          bottom: 0,
          zIndex: 100,
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: 20,
        }}>
        
        <View
          style={{
            height: 60,
            width: 70,
            backgroundColor: '#FFEDEE',
            borderRadius: 30,
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 10,
          }}>
          <Icon name="local-mall" style={{color: '#FF6670', fontSize: 25}} />
        </View>
        <View
          style={{
            height: 60,
            backgroundColor: '#6E8AFA',
            flex: 1,
            borderRadius: 30,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text onPress={() => props.navigation.navigate('Direction')} style={{fontSize: 15, color: '#fff', fontWeight: 'bold'}}>
            Menuju Lokasi
          </Text>
        </View>
      </View>
    );
  };
  return (
    <SafeAreaView style={{backgroundColor: '#fff', flex: 1}}>
      <ImageBackground
        source={{uri:imageUrl}}
        style={{
          height: 400,
          paddingTop: 40,
          paddingRight: 20,
          paddingLeft: 20,
        }}>
        <Image
          resizeMode="contain"
          style={{
            width: 100,
            marginBottom: 10,
          }}
          source={{uri:imageUrl}}
        />
        <Text
          style={{
            fontSize: 25,
            fontWeight: 'bold',
            top: -35,
            color:"grey"
          }}>
          {title}
        </Text>
        <View style={{top: -25, flexDirection: 'row'}}>
          <View style={{flexDirection: 'row', marginRight: 10}}>
            <Icon
              name="people"
              size={25}
              style={{color: '#61688B', marginRight: 5}}
            />
            <Text style={{color: '#61688B', fontWeight: 'bold'}}>
             
            </Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="star"
              size={25}
              style={{color: '#61688B', marginRight: 5}}
            />
            <Text style={{color: '#61688B', fontWeight: 'bold'}}>
             
            </Text>
          </View>
        </View>
      
      </ImageBackground>
      <View
        style={{
          flex: 1,
          marginTop: -35,
          backgroundColor: '#fff',
          // borderTopRightRadius: 50,
          // borderTopLeftRadius: 50,
          height: '10%',
        }}>
        {/* <Text
          style={{
            marginTop: 40,
            marginBottom: 20,
            marginHorizontal: 20,
            fontSize: 21,
            fontWeight: 'bold',
          }}>
          Deskripsi
        </Text> */}
        <ScrollView>
        <Text style={styles.description}>{description}</Text>
        </ScrollView>
      </View>
      <BottomNavigationBar />
    </SafeAreaView>
  );
};

export const DetailScreenOptions = (navData) => {
  return {
    headerTitle: navData.route.params.title,
    headerStyle: {
      backgroundColor: Platform.OS === "android" ? Color.accentColor : "",
    },
    headerTintColor: "#fff",
  };
};

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 300,
  },
  price: {
    fontSize: 20,
    color: "#888",
    marginVertical: 20,
    textAlign: "center",
    fontFamily: "open-sans-bold",
  },
  actions: {
    marginVertical: 10,
    alignItems: "center",
  },
  description: {
    fontFamily: "open-sans",
    fontSize: 14,
    textAlign: "justify",
    marginHorizontal: 20,
  },

});

export default DetailScreen;
