import React,{useState,useCallback,useEffect} from "react";
import { View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Alert,
  ActivityIndicator } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import * as LoginAction from '../store/action'
import Color from "../Color/Color";

const Login = (props) => {
  const dispatch = useDispatch()
  const authuser = useSelector((state) => state.auth.users);
  const authadmin = useSelector((state) => state.auth.admin);
  const [isSignup, setIsSignup] = useState(true);
  const [error, setError] = useState();
  const [loading, setisloading] = useState(false);
  const [login, setlogin] = useState({
    username: "",
    password: "",
  });

  const LoginHandler = useCallback(async () => {
    setError(null)
    setisloading(true);
    try {
        if(isSignup){
          await dispatch(LoginAction.authenticate(login))
        }else{
          await dispatch(LoginAction.authenticateadmin(login))
        }
        await props.navigation.navigate("Dashboard");
        setisloading(false)
    } catch (error) {
      setError(error.message);
      setisloading(false);
    }
  }, [login,dispatch,error]);


  useEffect(()=>{
    if(error){
      Alert.alert('An Error Occured!', error,[{
        text:'Okay'
      }])
    }
},[error])
  
return (
  <View style={styles.container}>
    <Text style={styles.logo}>Lokasi Wisata</Text>
    <View style={styles.inputView}>
      <TextInput
        style={styles.inputText}
        placeholder="Email.."
        placeholderTextColor="#003f5c"
        onChangeText={(text) => {
          setlogin({ ...login, username: text });
        }}
      />
    </View>
    <View style={styles.inputView}>
      <TextInput
        secureTextEntry
        style={styles.inputText}
        placeholder="Password.."
        placeholderTextColor="#003f5c"
        onChangeText={(text) => {
          setlogin({ ...login, password: text });
        }}
      />
    </View>
    <TouchableOpacity style={styles.loginBtn} onPress={LoginHandler}>
      {loading ? <ActivityIndicator size="small" color="white"/> :<Text style={styles.loginText}>{`${isSignup ? 'Login' : 'Daftar'}`} </Text>}
    </TouchableOpacity>
    
      {loading.loadingSign ?  <ActivityIndicator size="small" color="white"/>  : <Text style={styles.loginText} onPress={() => 
              setIsSignup(prevState => !prevState)
            }>Signup</Text>}

  </View>
);
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#003f5c",
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    fontWeight: "bold",
    fontSize: 50,
    color: "#fb5b5a",
    marginBottom: 40,
  },
  inputView: {
    width: "80%",
    backgroundColor: "#465881",
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    padding: 20,
  },
  inputText: {
    height: 50,
    color: "white",
  },
  loginBtn: {
    width: "80%",
    backgroundColor: "#fb5b5a",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10,
  },
  loginText: {
    color: "white",
  },
  forget: {
    color: "white",
    fontSize: 11,
  },
});
export default Login;
