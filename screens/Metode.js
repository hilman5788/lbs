import React, { useState, useRef } from "react";
import { View, Text, StyleSheet,Image,Dimensions } from "react-native";
import Carousel from 'react-native-snap-carousel';
import MapView, { PROVIDER_GOOGLE, Marker, Callout,Polygon,Polyline } from "react-native-maps";
import { set } from "react-native-reanimated";
const Metode = (props) => {
  const mapView = useRef();
  const [markers,setmarkers] =useState([])
  const [coordinates, setCoordinates] = useState([
    {
      latitude: -6.875046,
      longitude: 107.743869,
    },
    {
      latitude: -6.773272,
      longitude: 107.555727,
    },
  ]);
  const cords=[
      {name:'Gunung Manglayang',latitude:-6.875046,longitude:107.743869,images:"https://anekatempatwisata.com/wp-content/uploads/2016/08/Gunung-Bromo-Jawa-Timur.jpg"},
      {name:'Gunung Burangrang',latitude:-6.773442,longitude:107.555513,images:"https://anekatempatwisata.com/wp-content/uploads/2016/08/Gunung-Prau-Jawa-Tengah.jpg"},
      {name:'Gunung Bukit Tunggul',latitude:-6.810196,longitude:107.726944,images:"https://anekatempatwisata.com/wp-content/uploads/2016/08/Gunung-Rinjani-NTB.jpg"},
      {name:'Gunung Mandalawangi',latitude:-7.057246,longitude:107.863505,images:"https://anekatempatwisata.com/wp-content/uploads/2016/08/Gunung-Bromo-Jawa-Timur.jpg"},
      {name:'Gunung Kendang',latitude:-7.244767,longitude:107.708439,images:"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRVab83zA_FjW2Iq3Os1nEMyeTQ6zenanH2Rw&usqp=CAU"},
  ]

  const onCarouselItemChange = (index) => {
    let location = cords[index];
    console.log('locato',location)
    _map.animateToRegion({
      latitude: location.latitude,
      longitude: location.longitude,
      latitudeDelta: 0.09,
      longitudeDelta: 0.035
    })
  }

  const renderCorusel = ({item})=>{
      console.log('iem',item.images)
      return(
       <View style={styles.cardContainer}>
           <Text style={styles.cardTitle}>{item.name}</Text>
           <Image style={styles.cardImage} source={{
               uri:item.images
           }}/>
       </View>
      )
  }
  return (
      <View style={styles.container}>
    <MapView
      ref={map => _map = map}
      provider={PROVIDER_GOOGLE}
      style={styles.map}
      region={{
        latitude: -6.875046,
        longitude: 107.743869,
        latitudeDelta: 0.89,
        longitudeDelta: 0.035,
      }}
    >
       {/* <Polygon
       cordinates={{latitude: -6.875046,
        longitude: 107.743869,
        latitudeDelta: 0.89,
        longitudeDelta: 0.035,}}
      />  */}
    <Polyline 
     coordinates={cords}
    />
      <Marker
        coordinate={{
          latitude: -6.875046,
          longitude: 107.743869,
          latitudeDelta: 0.89,
          longitudeDelta: 0.035,
        }}
        title={"Gunung Burrangrang"}
      >
       <Callout>
           <Text>Gunung Manglayang</Text>
       </Callout>

      </Marker>
      {cords.map((marker,index)=>(
           <Marker
           key={index}
           coordinate={{latitude:marker.latitude,longitude:marker.longitude}}
           title={marker.name}
           >
                <Callout>
                     <Text>{marker.name}</Text>
                </Callout>
           </Marker>       
      ))}
    </MapView>
    <Carousel
              //ref={(c) => { carousel = c; }}
              data={cords}
              renderItem={renderCorusel}
              containerCustomStyle={styles.carousel}
              sliderWidth={Dimensions.get('window').width}
              itemWidth={300}
              onSnapToItem={(index) => onCarouselItemChange(index)}
            />
    </View>
  );
};

const styles = StyleSheet.create({
  container:{
      ...StyleSheet.absoluteFillObject
  },
  conta: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  carousel:{
    position: 'absolute',
    bottom: 0,
    marginBottom: 10
  },
  cardContainer:{
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    height: 200,
    width: 300,
    padding: 24,
    borderRadius: 24
  },
  cardTitle:{
    color: 'white',
    fontSize: 22,
    alignSelf: 'center'
  },
  cardImage:{
    height: 120,
    width: 300,
    bottom: 0,
    position: 'absolute',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24
  }


});

export default Metode;
