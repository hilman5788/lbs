import React from "react";
import HeaderButton from "../components/HeaderButton";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { DrawerActions } from "@react-navigation/native";
//import React from 'react';
import {View, SafeAreaView, StyleSheet} from 'react-native';
import {
  Avatar,
  Title,
  Caption,
  Text,
  TouchableRipple,
} from 'react-native-paper';
import  Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Color from "../Color/Color";

const Info = (props) => {
  const favorite =[
    {
      id: 1,
      description:
        "Bagi Toppers yang berada di daerah Jakarta Selatan, dan butuh referensi tempat wisata sejarah, Museum Satria Mandala adalah pilihannya Museum ini merupakan satu-satunya tempat bersejarah di Jakarta Selatan yang populer dan ramai dikunjungi orang,lho Sejarahnya, museum ini merupakan kediaman Ratna Sari Dewi, istri presiden pertama RI, Soekarno. Rumah ini bahkan menjadi tempat persemayaman terakhir Presiden Soekarno sebelum dikuburkan di kota Blitar.Saat Presiden Soeharto berkuasa, pada tahun 1971 rumah ini resmi menjadi Museum Satria Mandala dan dialihfungsikan sebagai pusat sejarah TNI. Museum ini berlokasi di Jl. Jend. Gatot Subroto No. 14, RT.6/RW.1, Kuningan Barat, Jakarta Selatan.Harga masuknya juga murah, Toppers, yakni senilai Rp 2000 per orang dan dibuka dari jam 09.00 sampai 14.30.",
      imageUrl:
        "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/08/museum-satria-mandala-1024x768.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      price: 2209,
      title: "Museum Satria Mandala, Jakarta Selatan",
    },
    {
      id: 2,
      description:
        "Monumen Nasional (Monas) ini merupakan tempat bersejarah yang menjadi simbol Jakarta dan Indonesia.Di area monumen ini terdapat banyak fasilitas rekreasi, lho. Contohnya seperti pelataran puncak monas di mana kita dapat melihat pemandangan Jakarta melalui teropong.Ada juga 51 diorama yang menggambarkan sejarah Indonesia mulai dari zaman pra-sejarah, kerajaan, kolonial, hingga saat ini. Bagi Toppers yang ingin mengunjungi Monas, lokasinya mudah dijangkau yaitu di Gambir, Jakarta Pusat.Harga masuknya pun tidak mahal. Untuk anak-anak senilai Rp 2.000, pelajar Rp 3.000, dan orang dewasa Rp 5.000",
      imageUrl:
        "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/08/monas.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      price: 1650,
      title: "Monumen Nasional, Jakarta Pusat",
    },
    {
      id: 3,
      description:
        "Jika kamu sedang berkunjung ke Kota Tua, jangan lupa untuk mengunjungi Museum Fatahillah. Dengan E-Money, kamu bisa mengunjungi tempat ini menggunakan TransJakarta atau KRL.Museum ini memberikan nuansa nostalgia yang kuat karena terdapat bangunan yang belum diubah sejak dibangun pada masa kolonial Belanda. Gedung ini dibangun oleh Jenderal Jan Pieterszoon Coen pada tahun 1620. Museum ini memiliki 23.500 benda bersejarah yang dapat dinikmati dari periode yang berbeda-beda.Harga masuk museum juga tidak mahal, yakni Rp 2.000 untuk anak-anak/pelajar, Rp 3.000 untuk mahasiswa, dan Rp 5.000 untuk pengunjung dewasa",
      imageUrl:
        "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/08/Museum-Fatahillah.jpg",
      price: 2608,
      title: "Museum Fatahillah, Jakarta Barat",
    },
    {
      id: 4,
      description:
        "Nah, buat para Toppers yang ingin wisata sejarah sambil foto-foto bernuansa vintage, museum inilah jawabannya.Tempat bersejarah di Jakarta ini dulu merupakan bangunan bekas komplek gudang untuk menyimpan rempah seperti pala, tembakau, kopra, lada, dan berbagai rempah lainnya.Banyak koleksi yang dapat dilihat di museum Bahari. Salah satunya adalah model-model kapal milik Belanda lengkap dengan meriamnya.Harga tiket masuk museum ini juga sangat murah, untuk pengunjung dewasa Rp 2.000, mahasiswa, pelajar, dan anak-anak Rp 1.000",
      imageUrl:
        "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/08/Museum-Bahari.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      price: 2434,
      title: "Museum Bahari, Jakarta Utara",
    },
    {
      id: 5,
      description:
        "Di Museum Bank Indonesia, kamu bisa menemukan benda dan dokumen bersejarah yang dirawat mengedukasi generasi penerus tentang sejarah Jakarta dan Indonesia.Di tempat bersejarah satu ini, Toppers juga bisa menemukan diorama menarik yang menceritakan perekonomian Indonesia dari waktu ke waktu.Selain itu, kamu juga bisa melihat mata uang rupiah dari masa ke masa serta peninggalan sejarah lainnya.Bagi Toppers yang ingin berkunjung, museum ini berlokasi di Jakarta Barat dan beroperasi dari jam 08.00 hingga jam 15.30",
      imageUrl:
        "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/08/Museum-Bank-Indonesia.jpg",
      ownerId: "7IzIGr0ypPNu1sMXUYjYVolqjHg1",
      price: 2208,
      title: "Museum Bank Indonesia, Jakarta Barat",
    },
  ];
  // return (
  //   <SafeAreaView style={styles.container}>
  //     <ScrollView showsVerticalScrollIndicator={false}>
  //       <View style={{ alignSelf: "center" }}>
  //         <View style={styles.profileImage}>
  //           <Image
  //             source={{uri: "https://www.kindpng.com/picc/b/136/1369892.png"}}
  //             style={styles.image}
  //             resizeMode="center"
  //           ></Image>
  //         </View>
  //         <View style={styles.add}>
  //           <MaterialIcons
  //             name="verified-user"
  //             color="black"
  //             size={48}
  //             color="#DFD8C8"
  //             style={{ marginTop: 6, marginLeft: 2 }}
  //           />
  //         </View>
  //       </View>

  //       <View style={styles.infoContainer}>
  //         <Text style={[styles.text, { fontWeight: "200", fontSize: 36 }]}>
  //          NAMA
  //         </Text>
  //         <Text style={[styles.text, { color: "#AEB5BC", fontSize: 14 }]}>
  //         NPM
  //         </Text>
  //       </View>

  //       <View style={styles.statsContainer}>
  //         <View
  //           style={[
  //             styles.statsBox,
  //             {
  //               borderColor: "#DFD8C8",
  //               borderLeftWidth: 1,
  //               borderRightWidth: 1,
  //             },
  //           ]}
  //         >
  //           <Text style={[styles.text, { fontSize: 24 }]}>WISATA</Text>
  //           <Text style={[styles.text, styles.subText]}>WISATA YANG MENJADI OBJEK PENELITIAN</Text>
  //         </View>
  //       </View>

  //       <View style={{ marginTop: 32 }}>
  //         <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
  //           {favorite.map((fav) => (
  //               //console.log(fav)
  //             <View style={styles.mediaImageContainer} key={fav.id}>
  //               <Image
  //                 source={{ uri: fav.imageUrl }}
  //                 style={styles.image}
  //                 resizeMode="cover"
  //               ></Image>
  //             </View>
  //           ))}
  //         </ScrollView>
  //       </View>
  //       <Text style={[styles.subText, styles.recent]}>Information</Text>
  //       <View style={{ alignItems: "center" }}>
  //         <View style={styles.recentItem}>
  //           <View style={styles.activityIndicator}></View>
  //           <View style={{ width: 250 }}>
  //             <Text
  //               style={[styles.text, { color: "#41444B", fontWeight: "300" }]}
  //             >
  //               Email:
  //               <Text style={{ fontWeight: "400" }}>emailpenulis@mail.com</Text>
  //             </Text>
  //           </View>
  //         </View>
  //       </View>
  //     </ScrollView>
  //   </SafeAreaView>
  // );
  return (
    <SafeAreaView style={styles.container}>

      <View style={styles.userInfoSection}>
        <View style={{flexDirection: 'row', marginTop: 15}}>
          <Avatar.Image 
            source={{
              uri: 'https://www.kindpng.com/picc/b/136/1369892.png',
            }}
            size={80}
          />
          <View style={{marginLeft: 20}}>
            <Title style={[styles.title, {
              marginTop:15,
              marginBottom: 5,
            }]}>Nama Penulis</Title>
          </View>
        </View>
      </View>

      <View style={styles.userInfoSection}>
        <View style={styles.row}>
          <Icon name="map-marker-radius" color="#777777" size={20}/>
          <Text style={{color:"#777777", marginLeft: 20}}>Alamat</Text>
        </View>
        <View style={styles.row}>
          <Icon name="phone" color="#777777" size={20}/>
          <Text style={{color:"#777777", marginLeft: 20}}>+91-900000009</Text>
        </View>
        <View style={styles.row}>
          <Icon name="email" color="#777777" size={20}/>
          <Text style={{color:"#777777", marginLeft: 20}}>john_doe@email.com</Text>
        </View>
      </View>

      <View style={styles.infoBoxWrapper}>
          <View style={[styles.infoBox, {
            borderRightColor: '#dddddd',
            borderRightWidth: 1
          }]}>
            <Title>10</Title>
            <Caption>Kota Wisata DI Jakarta</Caption>
          </View>
          <View style={styles.infoBox}>
            <Title>12</Title>
            <Caption>Lain Lain</Caption>
          </View>
      </View>

      <View style={styles.menuWrapper}>
        <TouchableRipple onPress={() => {}}>
          <View style={styles.menuItem}>
            <Icon name="heart-outline" color="#FF6347" size={25}/>
            <Text style={styles.menuItemText}>Kota Favorite</Text>
          </View>
        </TouchableRipple>
        <TouchableRipple onPress={() => {}}>
          <View style={styles.menuItem}>
            <Icon name="credit-card" color="#FF6347" size={25}/>
            <Text style={styles.menuItemText}>Semester</Text>
          </View>
        </TouchableRipple>
        <TouchableRipple onPress={() => {console.log('ok')}}>
          <View style={styles.menuItem}>
            <Icon name="share-outline" color="#FF6347" size={25}/>
            <Text style={styles.menuItemText}>NPM Atau Nik</Text>
          </View>
        </TouchableRipple>
        <TouchableRipple onPress={() => props.navigation.navigate("LoginScreen") }>
          <View style={styles.menuItem}>
            <Icon name="logout" color="#FF6347" size={25}/>
            <Text style={styles.menuItemText}>
              Logout
              </Text>
          </View>
        </TouchableRipple>
        
      </View>
      
    </SafeAreaView>
  );
};

export const InfoOption = (navData) => {
  return {
    headerTitle: "INFO PENULIS",
    headerStyle: {
      backgroundColor: Platform.OS === "android" ? Color.accentColor : "",
    },
    headerLeft: null,
    headerTintColor: Platform.OS === "android" ? "white" : Color.accentColor,
    // headerLeft: () => {
    //   return (
    //     <HeaderButtons HeaderButtonComponent={HeaderButton}>
    //       <Item
    //         title="Menu"
    //         iconName="ios-menu"
    //         onPress={() => {
    //           navData.navigation.dispatch(DrawerActions.openDrawer());
    //         }}
    //         color="white"
    //       />
    //     </HeaderButtons>
    //   );
    // },
    headerTitleStyle: {
      fontFamily: "open-sans-bold",
    },
    headerBackTitleStyle: {
      fontFamily: "open-sans",
    },
  };
};
const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   backgroundColor: "#FFF",
  // },
  // text: {
  //   fontFamily: "HelveticaNeue",
  //   color: "#52575D",
  // },
  // image: {
  //   flex: 1,
  //   height: undefined,
  //   width: undefined,
  // },
  // titleBar: {
  //   flexDirection: "row",
  //   justifyContent: "space-between",
  //   marginTop: 24,
  //   marginHorizontal: 16,
  // },
  // subText: {
  //   fontSize: 12,
  //   color: "#AEB5BC",
  //   textTransform: "uppercase",
  //   fontWeight: "500",
  // },
  // profileImage: {
  //   width: 200,
  //   height: 200,
  //   borderRadius: 100,
  //   overflow: "hidden",
  // },
  // dm: {
  //   backgroundColor: "#41444B",
  //   position: "absolute",
  //   top: 20,
  //   width: 40,
  //   height: 40,
  //   borderRadius: 20,
  //   alignItems: "center",
  //   justifyContent: "center",
  // },
  // active: {
  //   backgroundColor: "#34FFB9",
  //   position: "absolute",
  //   bottom: 28,
  //   left: 10,
  //   padding: 4,
  //   height: 20,
  //   width: 20,
  //   borderRadius: 10,
  // },
  // add: {
  //   backgroundColor: "#41444B",
  //   position: "absolute",
  //   bottom: 0,
  //   right: 0,
  //   width: 60,
  //   height: 60,
  //   borderRadius: 30,
  //   alignItems: "center",
  //   justifyContent: "center",
  // },
  // infoContainer: {
  //   alignSelf: "center",
  //   alignItems: "center",
  //   marginTop: 16,
  // },
  // statsContainer: {
  //   flexDirection: "row",
  //   alignSelf: "center",
  //   marginTop: 32,
  // },
  // statsBox: {
  //   alignItems: "center",
  //   flex: 1,
  // },
  // mediaImageContainer: {
  //   width: 180,
  //   height: 200,
  //   borderRadius: 12,
  //   overflow: "hidden",
  //   marginHorizontal: 10,
  // },
  // mediaCount: {
  //   backgroundColor: "#41444B",
  //   position: "absolute",
  //   top: "50%",
  //   marginTop: -50,
  //   marginLeft: 30,
  //   width: 100,
  //   height: 100,
  //   alignItems: "center",
  //   justifyContent: "center",
  //   borderRadius: 12,
  //   shadowColor: "rgba(0, 0, 0, 0.38)",
  //   shadowOffset: { width: 0, height: 10 },
  //   shadowRadius: 20,
  //   shadowOpacity: 1,
  // },
  // recent: {
  //   marginLeft: 78,
  //   marginTop: 32,
  //   marginBottom: 6,
  //   fontSize: 10,
  // },
  // recentItem: {
  //   flexDirection: "row",
  //   alignItems: "flex-start",
  //   marginBottom: 16,
  // },
  // activityIndicator: {
  //   backgroundColor: "#CABFAB",
  //   padding: 4,
  //   height: 12,
  //   width: 12,
  //   borderRadius: 6,
  //   marginTop: 3,
  //   marginRight: 20,
  // },
  container: {
    flex: 1,
  },
  userInfoSection: {
    paddingHorizontal: 30,
    marginBottom: 25,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    borderBottomColor: '#dddddd',
    borderBottomWidth: 1,
    borderTopColor: '#dddddd',
    borderTopWidth: 1,
    flexDirection: 'row',
    height: 100,
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
});

export default Info;
