export const LOGINUSER = "LOGINUSER";
export const LOGIADMIN = "LOGIADMIN";
export const LOGOUT = "LOGOUT";


export const authenticate = (login) => {
    return async(dispatch) => {
       const username=login.username;
       const password=login.password;
       const response = await fetch(
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyB40CI_oszrppz_e1yT6OAlPKGWraUivbs",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email: username,
            password: password,
            returnSecureToken: true,
          }),
        }
      );
  
      if (!response.ok) {
        const errorResData = await response.json();
        const errorId = errorResData.error.message;
        let message = "Something went wrong!";
        if (errorId === "EMAIL_NOT_FOUND") {
          message = "This email could not be found!";
        } else if (errorId === "INVALID_PASSWORD") {
          message = "This password is not valid!";
        }
        throw new Error(message);
      }
      const resData = await response.json();
      console.log(resData)
      dispatch({ type:LOGINUSER,data:{
          username:username,
          password:password
      } });
    };
};
export const authenticateadmin = (login) => {
    return async(dispatch) => {
       const username=login.username;
       const password=login.password;
      //console.log("login",login);
       const response = await fetch(
        "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyB40CI_oszrppz_e1yT6OAlPKGWraUivbs",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email: username,
            password: password,
            returnSecureToken: true,
          }),
        }
      );
  
      if (!response.ok) {
        const errorResData = await response.json();
        const errorId = errorResData.error.message;
        let message = "Something went wrong!";
        if (errorId === "EMAIL_EXISTS") {
          message = "This email exists already!";
        }
        throw new Error(message);
      }
  
      const resData = await response.json();
      console.log(resData);
  
      dispatch({ type:LOGIADMIN,data:{
          username:username,
          password:password
      } });
    };
};
export const logout = () => {
    return async(dispatch) => {
      dispatch({ type:LOGOUT});
    };
};