import { LOGIADMIN, LOGINUSER, LOGOUT } from "./action";

const intialState = {
  users:{
      username:"user@mail.com",
      password:"user"
  },
  admin:{
      username:"admin",
      password:"admin"
  },
  loginUser:{
      username:null,
      password:null
  },
  loginAdmin:{
      username:null,
      password:null
  }
};

export default (state = intialState, action) => {
  switch (action.type) {
    case LOGINUSER:  
       const {username,password}=action.data
       return{
           ...state,
           loginUser:{
               username:username,
               password:password
           }
       }
    case LOGIADMIN:  
       const usernames = action.data.username
       const passwords = action.data.password
       return{
           ...state,
           loginAdmin:{
               username:usernames,
               password:passwords
           }
       }
    case LOGOUT:  
       return {
           ...state,
           loginUser:{
            username:null,
            password:null
         },
         loginAdmin:{
            username:null,
            password:null
        }
    }
  }
  return state;
};
